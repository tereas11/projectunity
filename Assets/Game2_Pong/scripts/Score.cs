﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
	public static Score instance;

	[SerializeField] Text playerOneScoreText;
	[SerializeField] Text playerTwoScoreText; 

	public int playerOneScore;
	public int playerTwoScore;


	// Use this for initialization
	void Start () {

		instance = this;
		playerOneScore = playerTwoScore = 0;

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PlayerOnePoint() {
		playerOneScore += 1;

		playerOneScoreText.text = playerOneScore.ToString ();
	}

	public void PlayerTwoPoint() {
		playerTwoScore += 1;

		playerTwoScoreText.text = playerTwoScore.ToString ();
	}
}
