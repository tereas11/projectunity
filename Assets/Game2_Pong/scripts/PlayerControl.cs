﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

	[SerializeField] Transform LP;
	[SerializeField] Transform RP;
	[SerializeField] float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.W) && LP.position.y < 4.2) {
			LP.Translate (Vector2.up * Time.deltaTime * speed);
		}

		if (Input.GetKey (KeyCode.S) && LP.position.y > -4.0) {
			LP.Translate (Vector2.down * Time.deltaTime * speed);
		}
		if (Input.GetKey (KeyCode.UpArrow) && RP.position.y < 4.2) {
			RP.Translate (Vector2.up * Time.deltaTime * speed);
		}

		if (Input.GetKey (KeyCode.DownArrow) && RP.position.y > -4.0) {
			RP.Translate (Vector2.down * Time.deltaTime * speed);
		}
	}
}
