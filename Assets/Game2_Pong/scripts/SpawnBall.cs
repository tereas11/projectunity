﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBall : MonoBehaviour {
	
	Rigidbody rb;


	void Start (){
		//Get shortcut to rigidbody component
		rb = GetComponent<Rigidbody> ();

		//Pause ball, delay launch
		StartCoroutine (Pause ());

	}


	void Update(){
		//if ball goes too far left
		if (transform.position.x < -7.5f) {
			
			transform.position = Vector3.zero;
			rb.velocity = Vector3.zero;

			Score.instance.PlayerTwoPoint ();

			StartCoroutine (Pause ());

		}

		//ball goes too far right
		if (transform.position.x > 7.5f) {
			
			transform.position = Vector3.zero;
			rb.velocity = Vector3.zero;

			Score.instance.PlayerOnePoint ();
			
			StartCoroutine (Pause ());

		}

	}


	IEnumerator Pause () {
		
		//Wait
		yield return new WaitForSeconds (2f);

		//Call function Ball
		LaunchBall ();
	}


	void LaunchBall(){
		
		transform.position = Vector3.zero;
	
		//Ball Chooses a direction
		rb.velocity = new Vector3 (8f, 0f, 0f);

		//Random Direction
		int xDirection = Random.Range (0, 2);

		int yDirection = Random.Range (0, 3);

		Vector3 launchDirection = new Vector3 ();

		if (xDirection == 0) {
			launchDirection.x = -8f;
		}

		if (xDirection == 1) {
			launchDirection.x = 8f;
		}

		if (yDirection == 0) {
			launchDirection.y = -8f;
		}

		if (yDirection == 1) {
			launchDirection.y = 8f;
		}

		if (yDirection == 2) {
			launchDirection.y = 0;
		}


		//asigna velocidad basado a donde se lanza ball
		rb.velocity = launchDirection;

	}
		


}
		
