﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
	[SerializeField] Text timertext;
	private float startTime;
	[SerializeField] GameObject player;

	// Use this for initialization
	void Start () {
		startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		
		float x = Time.time - startTime;
		string seconds = (x % 60).ToString ("f2");

		timertext.text = "YOUR TIME :   " + seconds;

	}

}
