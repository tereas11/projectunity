﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoManejo : MonoBehaviour {

	private Rigidbody roberta;

	// Use this for initialization
	void Start () {
		roberta = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		roberta.AddForce (new Vector3 (0, 0, -ManejadorJuego.Gam.velochidad -ManejadorJuego.Gam.multiplier));
		}

	void OnTriggerEnter(Collider other)
	{
		Destroy (gameObject, 0.3f);
	}

}
