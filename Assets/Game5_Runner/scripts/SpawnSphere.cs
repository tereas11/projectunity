﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSphere : MonoBehaviour {
	
	[SerializeField] GameObject enemies;
	int Seed = 500;
	int Min = 0;
	int Max = 1000;

	private Transform transformer;

	void Start () {
		transformer = GetComponent <Transform> ();
	}
	

	void Update () {
		if (Seed == Random.Range (Min, Max)) {
			Instantiate (enemies, new Vector3 (transformer.transform.position.x, transformer.transform.position.y, transformer.transform.position.z), Quaternion.identity);
		}
	}
		
}
