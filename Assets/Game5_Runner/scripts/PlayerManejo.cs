﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManejo : MonoBehaviour {
	
	[SerializeField] GameObject explosion;
	Vector3 aqui = new Vector3 (0,1,0);
	public bool jump;


	private Rigidbody rbd;

	[SerializeField] Text gameoverText;


	void Start () {
		rbd = GetComponent<Rigidbody> ();
		jump = true;
	}
	
	void Update()
	{
		if (gameObject.transform.position.y < -4) {
			aqui += transform.position;
			Instantiate (explosion, aqui, Quaternion.identity);

			Destroy (gameObject);
			gameoverText.text = "GAME OVER!";
			Time.timeScale = 0;
		}
	}


//	IEnumerator Lane()
//	{
//	
//	}

	void FixedUpdate () {


		//----------MOVIMIENTO PLAYER-----
		if (rbd.velocity == new Vector3 (0, 0, 0)) {
			jump = false;
		}

		if (rbd.velocity != new Vector3 (0, 0, 0)) {
			jump = true;
		}

		//IZQUIERDA	
	

			if (Input.GetKeyDown (KeyCode.LeftArrow) && jump == false) {
			
				transform.Translate (new Vector3 (-2f, 0f, 0f));

			}

		//DERECHA

			if (Input.GetKeyDown (KeyCode.RightArrow) && jump == false) {
			
				transform.Translate (new Vector3 (2f, 0f, 0f));

			}

		//JUMP
		if (Input.GetKey (KeyCode.Space) && jump == false) {
			
				rbd.velocity = new Vector3 (0f, 15f, 0f);
			ManejadorJuego.Gam.multiplier = ManejadorJuego.Gam.multiplier + ManejadorJuego.Gam.velochidad;
		}
	}


	void OnTriggerEnter (Collider other){
		if (other.gameObject.CompareTag ("Enemy"))
		{
			aqui += transform.position;
			Instantiate (explosion, aqui, Quaternion.identity);

			Destroy (gameObject);

			gameoverText.text = "Game Over";
			Time.timeScale = 0;

		}
	}

}
