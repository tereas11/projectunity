﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
	[SerializeField]float speed;
	public Text countText;
	private int count;
	public Text winText;

	// Use this for initialization
	void Start () {
		count = 0;
		SetCountText ();
		winText.text = "";
	}

	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.W) && transform.position.y < 3.5) {
			transform.Translate (Vector2.up * Time.deltaTime * speed);
		}
		if (Input.GetKey (KeyCode.S) && transform.position.y > -3.5) {
			transform.Translate (Vector2.down * Time.deltaTime * speed);
		}
		if (Input.GetKey (KeyCode.D) && transform.position.x < 6.6) {
			transform.Translate (Vector2.right * Time.deltaTime * speed);
		}
		if (Input.GetKey (KeyCode.A) && transform.position.x > -6.5) {
			transform.Translate (Vector2.left * Time.deltaTime * speed);
		}

	}

	//Destruir Recolectable
	void OnTriggerEnter2D(Collider2D other) {

		if (other.gameObject.CompareTag ("PickUp")) {
			Destroy (other.gameObject);
			count = count + 1;
			SetCountText ();
		}
	}

	void SetCountText (){
		countText.text = "cookies: " + count.ToString ();
		if (count >= 10) {
			winText.text = "¡Ganaste!";
			Time.timeScale = 0;
		}
	}



}
