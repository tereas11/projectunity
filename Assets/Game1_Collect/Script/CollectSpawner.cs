﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectSpawner : MonoBehaviour {

	[SerializeField]GameObject cookie;


	// Use this for initialization
	void Start () {
		for (int i = 0; i < 10; i++) {
			float rndX = Random.Range (-7.5f,7f);
			float rndY = Random.Range (3.9f, -4.3f);
		
			Instantiate (cookie, new Vector3 (rndX,rndY,0), Quaternion.identity);
		}
	}
	
}