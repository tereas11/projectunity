﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerJump : MonoBehaviour {

	public static GameManagerJump Game;

	//GAMEMANAGER code
	void Awake () {
		if (Game == null) {
			Game = this;
			DontDestroyOnLoad (this);
		}
		else {
			Destroy (gameObject);
		}
	}	

}
