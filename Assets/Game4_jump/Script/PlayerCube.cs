﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCube : MonoBehaviour {
	
	private bool jump;
	private float minJump = 1f;
	private float maxJump = 10f;
	[SerializeField] float accumulatedForce = 0;
	Rigidbody rbd; 
	[SerializeField] GameObject escalon;

	[SerializeField] Text countText;
	[SerializeField] Text gameoverText;

	private int count = -1;

	float RnX = 7f;
	float RnY = 3f;



	// Use this for initialization
	void Start () {
		rbd = GetComponent<Rigidbody> ();
		jump = true;
	}

	void Update(){
	
		if (gameObject.transform.position.y < -3) {

			Destroy (gameObject);

			gameoverText.text = "GAME OVER T-T";


		}
	}


	private void FixedUpdate(){
		
		if (jump) {
			if (Input.GetButton ("Jump")) 
			{
				if (accumulatedForce < maxJump) 
				{
					accumulatedForce += Time.deltaTime * 10f;
				}
				//not holding jump button
				else 
				{
					accumulatedForce = maxJump;
				}
				//print (accumulatedForce);
			}
			//Jump
			else 
			{
				if (accumulatedForce > 0f) {
					accumulatedForce = accumulatedForce + minJump;
					rbd.velocity = new Vector3 (accumulatedForce * 0.5f, accumulatedForce, 0f);
					Invoke ("reset", 0.3f);
				}
			}
		}
	}


	void reset(){
		accumulatedForce = 0;
		jump = false;
	}


	void OnCollisionEnter(Collision other){
		if (other.gameObject.CompareTag ("ground")) 
		{
			jump = true;

			Instantiate (escalon, new Vector3 (RnX, RnY, 0), Quaternion.identity);

			RnX = RnX + 5f;
			RnY = RnY + 3f;
		
		}
		if (other.gameObject.CompareTag ("ground")) {
			count = count + 1;
			ScoreText();
		}

	}

	void OnCollisionExit(Collision other){
		Destroy (other.gameObject);
	}

	void ScoreText(){
		countText.text = "Score: " + count.ToString();
	}

}