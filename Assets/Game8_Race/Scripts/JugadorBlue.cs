﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JugadorBlue : MonoBehaviour {

	[SerializeField] Text GO;

	[SerializeField] float speed = 0;
	bool playerMoving;

	// Use this for initialization
	void Start () {
		GO.text = "";
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			StartCoroutine (OneMoveTo(1.09f));
		}
		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			StartCoroutine (OneMoveTo (2.96f));
		}
	}

	IEnumerator OneMoveTo(float tarX)
	{

		playerMoving = true;
		float distance = Vector2.Distance (transform.position, new Vector2 (tarX, 0));
		Vector2 tarPos = new Vector2 (tarX, transform.position.y);

		while (distance > 3f)
		{
			distance = Vector2.Distance (transform.position, new Vector2 (tarX, 0));

			if (transform.position.x > tarX )
			{
				transform.position = Vector2.MoveTowards (new Vector2 (transform.position.x, transform.position.y), tarPos, speed * Time.deltaTime);
			}

			else if (transform.position.x < tarX )
			{
				transform.position = Vector2.MoveTowards (new Vector2 (transform.position.x, transform.position.y), tarPos, speed * Time.deltaTime);
			}

			yield return new WaitForEndOfFrame ();
		}

		transform.position = new Vector3 (tarX, transform.position.y, transform.position.z);
		playerMoving = false;
	}

	void OnTriggerEnter2D (Collider2D other){
		if(other.gameObject.CompareTag ("suma a azul")){
			Destroy (other.gameObject);
			GMM.GM2.Score ();
		}

		if(other.gameObject.CompareTag ("resta a azul")){
			Debug.Log ("perdiste");
			Destroy (other.gameObject);
			GO.text = "GAME OVER";
			Time.timeScale = 0;
		}
	}


}


