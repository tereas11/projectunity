﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnR : MonoBehaviour {

	public GameObject[] monedas;
	bool crear = true;
	public Transform uno;
	public Transform dos;


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if(crear){
			Spawn();
			crear = false;
			Invoke ("Cooldown",3);
		}

	}

	void Spawn() { 
		int cual = Random.Range (0,2); 
		int donde = Random.Range (1,3);

		switch (donde) {
		case 1:
			Instantiate (monedas[cual], uno.position, Quaternion.identity);
			break;
		case 2:
			Instantiate (monedas[cual], dos.position, Quaternion.identity);
			break;
		default:

			break;
		}

	}
	void Cooldown(){
		crear = true;
	}

}
