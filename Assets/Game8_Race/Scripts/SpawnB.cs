﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnB : MonoBehaviour {

	public GameObject[] monedas;
	bool crear = true;
	public Transform tres;
	public Transform cuatro;


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if(crear){
			Spawn();
			crear = false;
			Invoke ("Cooldown",4);
		}

	}

	void Spawn() { 
		int cual = Random.Range (0,2); 
		int donde = Random.Range (1,3);

		switch (donde) {
		case 1:
			Instantiate (monedas[cual], tres.position, Quaternion.identity);
			break;
		case 2:
			Instantiate (monedas[cual], cuatro.position, Quaternion.identity);
			break;
		default:

			break;
		}

	}
	void Cooldown(){
		crear = true;
	}

}
