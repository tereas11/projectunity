﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverBlue : MonoBehaviour {

	[SerializeField] Text GO;

	// Use this for initialization
	void Start () {
		GO.text = "";	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other){
		if(other.gameObject.CompareTag ("suma a azul")){
			GO.text = "GAME OVER".ToString();
			Time.timeScale = 0;
		}
	}
}
