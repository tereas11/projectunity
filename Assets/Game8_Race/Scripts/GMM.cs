﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GMM : MonoBehaviour {

	public Text scoreJugador;

	public static GMM GM2;

	void Awake () {
		if (GM2 == null) {
			GM2 = this;
			DontDestroyOnLoad (this);
		}
		else {
			Destroy (gameObject);
		}
	}	

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	public void Score () {
		var textUI = GameObject.Find ("Score").GetComponent<Text> ();
		int score = int.Parse (textUI.text);
		score++;
		textUI.text = score.ToString ();
	}
}
