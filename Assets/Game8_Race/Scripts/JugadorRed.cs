﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JugadorRed : MonoBehaviour {
	

	[SerializeField] Text loseText;

	[SerializeField] float speed = 0;
	bool playerMoving;

	// Use this for initialization
	void Start () {
		loseText.text = "";
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.A)) {
			StartCoroutine (OneMoveTo(-3.2f));
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			StartCoroutine (OneMoveTo (-1f));
		}
	}

	IEnumerator OneMoveTo(float tarX)
	{

		playerMoving = true;
		float distance = Vector2.Distance (transform.position, new Vector2 (tarX, 0));
		Vector2 tarPos = new Vector2 (tarX, transform.position.y);

		while (distance > 3f)
		{
			distance = Vector2.Distance (transform.position, new Vector2 (tarX, 0));

			if (transform.position.x > tarX )
			{
				transform.position = Vector2.MoveTowards (new Vector2 (transform.position.x, transform.position.y), tarPos, speed * Time.deltaTime);
			}

			else if (transform.position.x < tarX )
			{
				transform.position = Vector2.MoveTowards (new Vector2 (transform.position.x, transform.position.y), tarPos, speed * Time.deltaTime);
			}

			yield return new WaitForEndOfFrame ();
		}

		transform.position = new Vector3 (tarX, transform.position.y, transform.position.z);
		playerMoving = false;
	}

	void OnTriggerEnter2D (Collider2D other){
		if(other.gameObject.CompareTag ("suma a rojo")){
			Destroy (other.gameObject);
			GMM.GM2.Score ();
		}

		if(other.gameObject.CompareTag ("resta a rojo")){
			Debug.Log ("perdiste");
			Destroy (other.gameObject);
			loseText.text = "GAME OVER";
			Time.timeScale = 0;
		}
	}
		
}
