﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static GameManager GM;

	[SerializeField] GameObject prefab;

	// Use this for initialization
	void Awake () {
		if (GM == null) {
			GM = this;
			DontDestroyOnLoad (this);
		}
		else {
			Destroy (gameObject);
		}
	}	

	//aparece enemigos
	void Start () {
		
		SpawnEnemy ();

	}


	void SpawnEnemy (){
		for (float x = -4.5f; x <= 4.5f; x++) {

			Instantiate (prefab, new Vector3 (x,0,0), Quaternion.identity);

			for (float y = 1; y <= 4; y++) {

				Instantiate (prefab, new Vector3 (x,y,0), Quaternion.identity);
			}
		}

	}



}
