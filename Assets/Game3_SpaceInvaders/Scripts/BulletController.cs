﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletController : MonoBehaviour {

	[SerializeField] float speedid;
	private Transform myshot;
	[SerializeField] Sprite explosion;


	void Start () {
		
		myshot = transform;

	}
		

	// Movimiento de la bala
	void Update () {
		
		float Move = speedid * Time.deltaTime;
		myshot.Translate (Vector3.up * Move);

	}


	//Al colisionar en un enemigo se destruye la bala
	void OnTriggerEnter2D(Collider2D other) {

		if (other.tag == "invader") 
		{
			//Destroy (other.gameObject);

			IncreaseTextUIScore ();

			other.GetComponent<SpriteRenderer> ().sprite = explosion;

			Destroy (gameObject);

			DestroyObject(other.gameObject, 0.3f);
		}

		if (other.tag == "obstacle") {
			Destroy (gameObject);
		}
	}
		

	//al desaparecer de la camara se destruye
	void OnBecameInvisible(){
		Destroy (gameObject);
	}


	void IncreaseTextUIScore()
	{
		var textUIComp = GameObject.Find ("Score").GetComponent<Text>();
		int score = int.Parse (textUIComp.text);

		score += 10;

		textUIComp.text = score.ToString ();

	}

}
