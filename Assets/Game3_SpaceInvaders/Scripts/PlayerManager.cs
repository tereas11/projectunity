﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

	[SerializeField]float speed;
	[SerializeField] GameObject prefab;
	[SerializeField] bool wait = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.LeftArrow) && transform.position.x > -6)
			transform.Translate(Vector2.left * Time.deltaTime * speed);
			
		if (Input.GetKey (KeyCode.RightArrow) && transform.position.x < 6)
			transform.Translate(Vector2.right * Time.deltaTime * speed);
	
		if (Input.GetKeyDown("space")&& wait ) 
		{
			wait = false;
			Invoke ("balaespera", .3f);

		}

	}

	void balaespera(){

		Instantiate (prefab,transform.position, Quaternion.identity);
		wait = true;
	}

}
