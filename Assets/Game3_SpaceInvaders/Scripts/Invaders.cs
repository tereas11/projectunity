﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invaders : MonoBehaviour {
	
	public float speed = 10;

	private Rigidbody2D rigidBody;

	[SerializeField] GameObject invaderBullet;

	[SerializeField] float minFireRateTime = 1.0f;

	[SerializeField] float maxFireRateTime = 3.0f;

	[SerializeField] float baseFireWaitTime = 3.0f;


	// Use this for initialization
	void Start () {

		rigidBody = GetComponent<Rigidbody2D>();

		rigidBody.velocity = new Vector2 (1, 0) * speed;

		baseFireWaitTime = baseFireWaitTime + Random.Range (minFireRateTime, maxFireRateTime);

	}

	//Turn in opposite direction
	void Turn(int direction){
		Vector2 newVelocity = rigidBody.velocity;
		newVelocity.x = speed * direction;
		rigidBody.velocity = newVelocity;
		 
	}

	//Move down after hitting wall
	void MoveDown(){
		Vector2 position = transform.position;
		position.y -= 0.5f;
		transform.position = position;
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.name == "LeftWall") 
		{
			Debug.Log ("left collision");
			Turn (1);
			MoveDown();
		}

		if (other.gameObject.name == "RightWall") 
		{
			Debug.Log ("right collision");
			Turn (-1);
			MoveDown();
		}
		if (other.gameObject.name == "Bullet") 
		{
			Destroy (gameObject);
		}
	}
		
		

	void FixedUpdate()
	{
		if (Time.time > baseFireWaitTime) {
			baseFireWaitTime = baseFireWaitTime + Random.Range (minFireRateTime, maxFireRateTime);

			Instantiate (invaderBullet, transform.position, Quaternion.identity);
		}
	}


	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.name == "player") {
			Destroy (other.gameObject);
		}

	}





}
