﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BulletInvaders : MonoBehaviour {

	[SerializeField] float speed;
	private Transform shot;
	[SerializeField] Sprite explotedShip;


	void Start () {

		shot = transform;
	}

	// Movimiento bullet
	void Update () {

		float Move = speed * Time.deltaTime;
		shot.Translate (Vector3.down * Move);

	}


	void OnTriggerEnter2D(Collider2D col){
		
		if (col.tag == "Wall") 
		{
			Destroy (gameObject);
		}

		if (col.gameObject.tag == "spaceship") 
		{
			col.GetComponent<SpriteRenderer> ().sprite = explotedShip;

			Destroy (gameObject);
			DestroyObject (col.gameObject, 0.3f);
			Time.timeScale = 0;
		}

		if (col.tag == "obstacle") 
		{
			Destroy (gameObject);
			DestroyObject (col.gameObject);
		}

	}
		

	void OnBecameInvisible(){
		Destroy (gameObject);
	}


}
