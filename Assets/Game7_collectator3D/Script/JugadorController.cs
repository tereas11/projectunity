﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JugadorController : MonoBehaviour {

	public float speed;

	private Rigidbody rb;

	private int count;

	[SerializeField] Text countText;
	[SerializeField] Text winText;

	void Start ()
	{
		rb = GetComponent<Rigidbody>();
		count = 0;
		SetCountText ();
		winText.text = " ";
	
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

		rb.AddForce (movement * speed);
	}

	void OnTriggerEnter (Collider other){
		if (other.gameObject.CompareTag ("PickUp")) {
			Destroy(other.gameObject);
			count = count + 1;
			SetCountText ();

		}
	}

	void SetCountText (){
		countText.text = " " + count.ToString (); 
		if (count >= 10) {
			winText.text = "WINNER!";
		}
	}
}


