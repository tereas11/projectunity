﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Operaciones : MonoBehaviour {

	public int operacion; //variable que cambia, intentar en bite
	public Button sumar; //boton +1
	public Button restar; //boton -1
	Text text; //imprime el resultado

	// Use this for initialization

	void Start () {
		//para accecar publicamente al componente
		Button botonsumar = sumar.GetComponent<Button> (); 
		Button botonrestar = restar.GetComponent<Button> ();
		text = GetComponent<Text> ();

		botonsumar.onClick.AddListener (Agregar); //ButtonClickedEvent triggered
		botonrestar.onClick.AddListener (Quitar);
	}

	//Funcion Sumar Event
	void Agregar (){
		operacion = operacion + 1;
		Debug.Log("sumaste");
		text.text = "Sumaste: " + operacion ;
	}

	//Funcion Restar Event
	void Quitar (){
		operacion = operacion - 1;
		Debug.Log("restaste");
		text.text = "Restaste: " + operacion;
	}


}
