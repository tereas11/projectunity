﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerTubos : MonoBehaviour {
	public int SpawnMin;
	public int SpawnMax;
	public int MinHeight;
	public int MaxHeight;
	public GameObject pipe;

	// Use this for initialization
	void Start () {
		StartCoroutine ("Spawn");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator Spawn(){
		yield return new WaitForSeconds (Random.Range (SpawnMin, SpawnMax));

		Instantiate (pipe, new Vector3 (this.transform.position.x, Random.Range (MinHeight, MaxHeight), 
			this.transform.position.z), this.transform.rotation);

		StartCoroutine ("Spawn");
	}



}
