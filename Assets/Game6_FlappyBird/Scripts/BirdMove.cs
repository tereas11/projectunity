﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BirdMove : MonoBehaviour {

	Vector3 velocity = Vector3.zero;
	public Vector3 gravity;
	public Vector3 flapVelocity;
	public float maxSpeed = 5f;

	[SerializeField] GameObject GO;
	public Transform Aqui;
	[SerializeField] Text countText;
	private int count;

//	Rigidbody rebelde;

	bool Flap = false;


	// Use this for initialization
	void Start () {
//		rebelde = GetComponent <Rigidbody> ();
		count = 0;
		SetCountText ();
	}


	void Update(){
		if (Input.GetKeyDown (KeyCode.Space) || Input.GetMouseButtonDown (0)) {
			Flap = true;
		}
	}


	// Update is called once per frame
	void FixedUpdate () {
		velocity += gravity * Time.deltaTime;

		if (Flap == true) {

			Flap = false;
			if (velocity.y < 0)
				velocity.y = 0;
		
			velocity += flapVelocity;
		}

		transform.position += velocity * Time.deltaTime;

	}

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.CompareTag ("Tubo")) {
			Debug.Log ("choquee");
			Destroy (gameObject);
			Instantiate (GO, Aqui.position, Aqui.rotation);
//		rebelde.velocity = Vector3.zero;
			Time.timeScale = 0;
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		count = count + 1;
		SetCountText ();
	}

	void SetCountText (){
		countText.text = "" + count.ToString ();
	}

}
